# Hardening wiht ansible

Provision hardening

``` shell
ansible-playbook -v -i <ip>, --user <user> hardening.yml
```

## Lynis scan

Run the scan

``` shell
ansible-playbook -v -i <ip>, --user <user> hardening.yml
```

Clear the results

``` shell
ansible-playbook -v -i <ip>, --user <user> hardening.yml -e "remove_lynis=yes"
```

